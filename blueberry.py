#!/usr/bin/python

#
#  Blueberry is a program to connect to the raspberry pi serial port on /dev/ttyAMA0. Such port is connected to a bluetooth serial port.
#  It parses the input on the serial port, to get commands and its values to control the camera board. It is used to take time-lapses and photos
#  It provides control communication for the pi/camera when the pi is not connected to a keyboard/display/wifi/ethernet.
#
#
#  List of commands:
#
#   CMD                                                     Description                             EXAMPLE USE
#   ----                                                    -----------------------------          -----------------------------------
#   S00                                                     Shutdown the pi. No extra argument     S00;
#   R00                                                     Reboot the pi.                         R00;
#   STL{lapse},{FRAME-TIME},{JPG Quality},{fn-prepend}      Start a timelapse capture.             STL,10,1,95,playa; => For 10 minutes, a frame each 1minute.
#   SVC{lapse}                                              Start a video capture.                 SVC,10;   => For 10 minutes.
#   SHT                                                     Capture a STILL. No extra args.        SHT;
#   


import time
import serial
import os
import sys
import subprocess
import picamera


import logging
import logging.handlers

from subprocess import * 


#The LCD Char CODE from ADAFRUIT.
import libs
from libs.adafruit_charlcd import Adafruit_CharLCD

commands = {
    'S00': 'SHUTDOWN',
    'R00': 'REBOOT',
    'STL': 'START_TIMELAPSE',
    'SVC': 'START_VIDEO',
    'SHT': 'CAPTURE_STILL',
}


def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output


def readLine(port):
    buf = ""
    while 1:
        c = ""
        c = port.read() # this blocks the program. Use timeout when openning the port
        logger.info("%s | Reading serial port: '%s' | %xh | %s d"%(ipaddr,c, ord(c), ord(c)))
        if (c != ';'):
            # each command is separated by a semicolon.
            #ignore ack,startFlag,abord CHARS
            if (ord(c) == 18):
                logger.info("Getting the startFlag. CMD send by the Amarino library. Ignoring")
            elif (ord(c) == 19):
                logger.info("Getting the ackFlag. CMD send by the Amarino library. Ignoring")
            elif (ord(c) == 27):
                logger.info("Getting the abordFlag. CMD send by the Amarino library. Ignoring")
            else:
                buf += c
                if (buf != ""):
                    lcd.clear()
                    lcd.message("IP: %s\nCMD:%s"%(ipaddr,buf))
        else :
            return buf #returns the buffer ignoring the ';'


def processCommand(cmd):
    # Parse commands, each command signature is formed by 3 chars.
    # Each command can contain some arguments, separated by a colon.
    logger.info("Processing commnand %s"%(cmd))
    cmdLen = len(cmd)
    bt.write("%s | Processing command: %s"%(ipaddr, cmd))
    if (cmdLen >= 3):
        try:
            cmdSign = cmd[0:3]
        except(ValueError, TypeError):
            logger.info("Exception when getting Command Signature")
            return

        logger.info('Command signature: %s'%cmdSign)
        if (cmdSign in commands):
            if (cmdSign == "S00"):
                #Shutdown command
                logger.info("S00: Shutting down")
                bt.write("SHUTTING DOWN!...")
                lcd.clear()
                lcd.message("IP: %s\n%s"%(ipaddr,"Shutdown..."))
                os.system("shutdown -h now")
            elif (cmdSign == "R00"):
                #Reboot command
                logger.info("R00: Rebooting")
                bt.write("REBOOTING...")
                lcd.clear()
                lcd.message("IP: %s\n%s"%(ipaddr,"Reboot..."))
                os.system("reboot")
            elif (cmdSign == "STL"):
                #Start Time-lapse capture. This needs the arguments for the time-lapse options
                logger.info("STL: Starting Time-lapse.")
                bt.write("Stargin Time-lapse.")
                lcd.clear()
                lcd.message("IP: %s\n%s"%(ipaddr,"Time Lapse..."))
                start_timelapse(cmd)
            elif (cmdSign == "SVC"):
                #Start Video capture
                pass
            elif (cmdSign == "SHT"):
                #Capture a still image
                lcd.clear()
                lcd.message("IP: %s\n%s"%(ipaddr,"STILL Capture..."))
            else:
                pass
        else:
            logger.info("Command NOT supported.")
            bt.write("Command NOT SUPPORTED")
            lcd.clear()
            lcd.message("IP: %s\n%s"%(ipaddr,"Cmd NOT SUPPORTD"))


    else:
        logger.info('GOT A STRING without a command: %s'%cmd)
        bt.write("NO COMMAND RECOGNISED")
        lcd.clear()
        lcd.message("IP: %s\n%s"%(ipaddr,"NO Command..."))


def start_timelapse(cmd):
    #  STL20,1. Time is given in MINUTES.
    args = cmd[3:].split(',')
    logger.info("STL: args=%s"%args)
    #first argument is the lapse time, second argument is the frame-time
    try:
        lap = int(args[0])*60  #convert to seconds.. to aling with the ftime operations
        ftime = int(args[1])*60 # Minutes converted to seconds for use in time.sleep()
        quality = int(args[2])
        fn_pre = args[3]
    except(ValueError, TypeError):
        logger.info('STL: Unexpected arguments: %s'%args)
        lcd.clear()
        lcd.message("IP: %s\n%s"%(ipaddr,"Unexpected ARGS"))
        return

    if (lap < ftime):
        logger.info('LAPSE is shorter than frame time. Plese increase it or lower the frame time.')
        bt.write("LAPSE is shorter that frame time. Please increase it or lower the frame time.")
        lcd.clear()
        lcd.message("IP: %s\n%s"%(ipaddr,"LAPSE < F.TIME"))
        return

    try:
        times = int(lap/ftime)
    except:
        logger.info('STL: Error on lap/time: %s/%s'%(lap,time))
        bt.write("STL: Error on lap/time: %s/%s"%(lap,time))
        lcd.clear()
        lcd.message("IP: %s\n%s"%(ipaddr,"ERROR on lap/time: %s/%s"%(lap,time)))
        return

    logger.info("STL: LAP=%s, FRAME-TIME:%s, #Frames=%s, Quality:%s"%(lap,ftime,times,quality))
    bt.write("STL: LAP=%s, FRAME-TIME:%s, #Frames=%s, Quality:%s"%(lap,ftime,times,quality))
    lcd.clear()
    lcd.message("%s\n%s"%("Duration: %s Lap:%s"%(lap,ftime), "Quality:%s #F:%s"%(quality,times)))
    # continue with the capture
    with picamera.PiCamera() as camera:
        camera.resolution = (2592,1944)
        camera.exif_tags['IFD0.Copyright'] = 'Copyright (c) 2014 Miguel Chavez Gamboa'
        camera.exif_tags['EXIF.UserComment'] = 'Timelapse using raspberry pi'
        time.sleep(2)
        try:
            #FIXME: How to name files to avoid rewrite? using date in filename.
            fn = '/home/miguel/cameraONE/time-lapses/%s-{timestamp}.jpg'%fn_pre
            for i,filename in enumerate(camera.capture_continuous(fn, quality=quality, bayer=True)):
                logger.info('STL: %s  Captured %s @%s' % (i,filename,camera.resolution))
                bt.write("STL: #%s"%(i))  #ESTO causa exception
                lcd.clear()
                #lcd.message("#%s @ %s\n%s-%s"%(i,camera.resolution,fn_pre,i))
                if (i >= times-1):
                    break
                time.sleep(ftime)
        except :
            e = sys.exc_info()[1]
            logger.info('ERROR: %s'%e)
            bt.write("ERROR when capturing: %s"%e)
            lcd.clear()
            lcd.message("%s\n%s"%("Error Capturing", e))
        finally:
            camera.close()
            #Change permissions to files
            os.system("chown -R miguel:miguel /home/miguel/cameraONE/time-lapses/")



#logger
#file_handler = logging.FileHandler('codeame.blueberry.log')
h = logging.handlers.RotatingFileHandler('/var/log/codeame.blueberry.log', maxBytes=10000, backupCount=5)
logger = logging.getLogger('codeame.blueberry')
logger.addHandler(h)
logger.setLevel(logging.DEBUG)


bt = serial.Serial('/dev/ttyAMA0', 9600) #, baudrate=115200, timeout=3.0
logger.info("Conecting to serial port...")
time.sleep(0)


lcd = Adafruit_CharLCD(pin_rs=7, pin_e=8, pins_db=[25, 24, 23, 18])
lcd.begin(16,2)

#it is exected once at start, If ip changes, then the change will not be reflected.
ipaddr = run_cmd("ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1")

while (ipaddr == ""):
    ipaddr = run_cmd("ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1")
    lcd.clear()
    lcd.message("Sleping 3...")
    #logger.info("Sleeping for 3 seconds...")
    time.sleep(3)

while 1:
    #First, write the status to the LCD.
    ipaddr = run_cmd("ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1")
    lcd.clear()
    lcd.message("IP: %s\n"%ipaddr)
    logger.info("IP: %s"%ipaddr)
    #--
    #Now read the bt serial port
    com = readLine(bt)
    #And finally process the command if any.
    processCommand(com)
